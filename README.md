# sshman
A tool to help you manage your different SSH connections.

Imagine sshman like a library to store different connection details.

## Usage
`sshman <command> <args>`

- Use `sshman ssh <alias>` to start a SSH connection
- Use `sshman scp <alias> <file or dir>` to start a file transfer using SCP
- Use `sshman bind <alias> <bind alias or bind description>` to create a SSH tunnel

**NOTE**: sshman expects the commands `ssh` and `scp` to be installed and in your
`$PATH`.

## Creating Aliases
If `XDG_CONFIG_HOME` is set, then sshman will read from `$XDG_CONFIG_HOME/sshman.conf`.
If, however, `XDG_CONFIG_HOME` is not set, then sshman will read from `~/.config/sshman.conf`.

The file is expected to be in JSON format and have the following structure:

```json
[
    {
       "alias": "Test", // The name for a connection
       "address": "ssh.example.com", // The address of the remote host
       "port": 22, // The port sshd is running on
       "user": "admin", // The user to connect as
       "key": "admin_key", // The key file to use. If the path contains no slashes, then tha value
                           // will be interpreted as the name of a key in ~/.ssh. If it contains
                           // slashes, then the value will be interpreted as the absolute path to a key
       "bind": "ipv4", // If set to "ipv4" then the -4 flag will be passed to ssh when doing a bind
       "binds": [ // A list of binds that can be addressed via an alias
          {
              "alias": "monitor", // The name of the bind
              "lport": 8080, // The port to bind locally on
              "remote": "127.0.0.1", // The address to direct the traffic from the remote host to
              "rport": 27017 // The port to direct the traffic on the remote host to
          },
          [...]
       ]
    },
    [...]
]
```

The file can be edited and an alias can be added, as long as it follows this format.
