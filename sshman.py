import os
import sys
from pathlib import Path
import json
import getpass
import shlex

def log(msg, tabs=0, type="m"):
    if type == "m":
        print("[*] {0}{1}".format("\t" * tabs, msg))
    elif type == "e":
        print("[E] {0}{1}".format("\t" * tabs, msg))
    elif type == "d":
        if os.getenv("DEBUG") != None:
            print("[D] {0}{1}".format("\t" * tabs, msg))

# Loads the config from "$HOME/.config/sshman.conf" and
# returns the JSON object
def load_config():
    # Try to honor XDG_CONFIG_HOME
    xdg = os.getenv("XDG_CONFIG_HOME")
    path = ""
    if xdg != None:
        path = os.path.join(xdg, "sshman.conf")
    else:
        # Build XDG_COFIG_HOME ourselves
        # Get the home directory
        home = str(Path.home())
        path = os.path.join(home, ".config", "sshman.conf")
    
    log("Loading config from {}".format(path), type="d")

    if not os.path.exists(path):
        log("Creating the config file under {}".format(path), type="d")
        open(path, "w").write("[]\r\n")
        return []
    
    data = json.loads(open(path, "r").read())
    return data

# Guesses whether @key is the path of a key or just the name
# of a key in "$HOME/.ssh"
def key_file_path(key):
    if "/" in key:
        # I guess that it's a path
        return key
    else:
        # Guess that it's the name of the key in ~/.ssh
        path = os.path.join(str(Path.home()), ".ssh", key)
        log("Returning '{}' as the key file path".format(path), type="d")
        return path

# Invokes the SSH command based on the data that is connected to the alias
def cmd_ssh(args, data):
    # Did we get a name
    if len(args) == 0:
        log("No alias specified", type="e")
        sys.exit(1)

    for connection in data:
        if connection["alias"] == args[0]:
            addr = connection["address"]
            port = connection["port"]
            user = connection["user"]
            log("Connecting to {}:{} as {}".format(addr, port, user))

            cmd = "ssh -p{0} -l{1} {2}"
            if "key" in connection:
                keyfile = key_file_path(connection["key"])
                cmd += " -i{}".format(shlex.quote(keyfile))

            cmd = cmd.format(
                shlex.quote(str(port)),
                shlex.quote(user),
                shlex.quote(addr),
            )

            log("SSH command: '{}'".format(cmd), type="d")
            os.system(cmd)
            
            return 0

    log("Could not find the alias '{}'".format(args[0]), type="e")
    return 1

# List all known aliases with their data
def cmd_list(args, data):
    for connection in data:
        display_entry(connection)

def display_entry(entry):
    log("[{0}]".format(entry["alias"]))
    log("Address:    {}".format(entry["address"]), tabs=1)
    log("Port:       {}".format(entry["port"]), tabs=1)
    log("Connect as: {}".format(entry["user"]), tabs=1)

    if "key" in entry:
        log("Key:        {}".format(entry["key"]), tabs=1)
    else:
        log("Key: NO", tabs=1)

# View a singular alias with its data 
def cmd_view(args, data):
     # Did we get a name
    if len(args) == 0:
        log("No alias specified", type="e")
        return 1

    for connection in data:
        if connection["alias"] == args[0]:
            display_entry(connection)
            return 0

    log("Did not find the alias {}".format(args[0]), type="e")
    return 1

# Tries to guess the directory to copy the file on the remote host to
def get_home_path(data):
    if "scp_dir" in data:
        return data["scp_dir"]

    return "/home/{}".format(data["user"])

def cmd_scp(args, data):
      # Did we get a name
    if len(args) < 2:
        log("No alias or file specified", type="e")
        return 1

    for connection in data:
        if connection["alias"] == args[0]:
            port = connection["port"]
            addr = connection["address"]
            user = connection["user"]

            cmd = "scp -P{0}"
            if "key" in connection:
                keyfile = key_file_path(connection["key"])
                cmd += " -i{}".format(shlex.quote(keyfile))

            home_path = get_home_path(connection)
            cmd += " {3} {1}@{2}:{4}"

            filepath = os.path.abspath(args[1])
            if not os.path.isfile(filepath):
                filepath = "-r {}".format(filepath)
            
            cmd = cmd.format(
                shlex.quote(str(port)),
                shlex.quote(user),
                shlex.quote(addr),
                filepath,
                home_path
            )

            log("Running SCP command: {}".format(cmd), type="d")
            log("Connecting to {0}:{1} as {2}".format(addr, port, user))
            os.system(cmd)
            
            return 0

    log("Did not find the alias {}".format(args[0]), type="e")
    return 1

def find(arr, f):
    for el in arr:
        if f(el) == True:
            return el

    return None

def cmd_bind(args, data):
       # Did we get a name
    if len(args) < 2:
        log("No alias or bind specified", type="e")
        return 1

    log("args: {}".format(args), type="d")
    
    for connection in data:
        if connection["alias"] == args[0]:
            port = connection["port"]
            addr = connection["address"]
            user = connection["user"]

            cmd = "ssh -p{0} -l{1}"
            if "key" in connection:
                keyfile = key_file_path(connection["key"])
                cmd += " -i{}".format(shlex.quote(keyfile))

            if "bind" in connection and connection["bind"] == "ipv4":
                cmd += " -4"
            cmd += " -N -L {3}:{4}:{5} {2}"

            bind_data = []
            if args[1].count(":") >= 3:
                raw = args[1].split(":")
                bind_data = [
                    raw[0], # Local port
                    raw[1], # Target on the remote host
                    raw[2]  # Target port
                ]
            else:
                log("Looking in the bind aliases", type="d")
                alias = find(connection["binds"], lambda x: x["alias"] == args[1])
                if alias == None:
                    log("Could not find bind alias '{}'".format(args[1]), type="e")
                    sys.exit(1)

                bind_data = [
                    alias["lport"],
                    alias["remote"],
                    alias["rport"]
                ]

            cmd = cmd.format(
                shlex.quote(str(port)),
                shlex.quote(user),
                shlex.quote(addr),
                shlex.quote(str(bind_data[0])),
                shlex.quote(bind_data[1]),
                shlex.quote(str(bind_data[2]))
            )

            log("Running SSH command: {}".format(cmd), type="d")
            log("Connecting to {0}:{1} as {2}".format(addr, port, user))


            log("Binding 127.0.0.1:{0} to {1}:{2} on the remote host".format(
                bind_data[0], bind_data[1], bind_data[2]))
            os.system(cmd)
            
            return 0

    log("Did not find the alias {}".format(args[0]), type="e")
    return 1

def cmd_help():
    print("sshman")
    print("\nCommands:")
    print("{}list . List all known connections".format("\t"))
    print("{}view . View the information of a connection".format("\t"))
    print("{}ssh  . Connect to the alias using ssh".format("\t"))
    print("{}scp  . Copy a file to the alias using scp".format("\t"))

def main():
    argv = sys.argv[1:]
    log("ARGV: {}".format(argv), type="d")
    
    # Check if we have arguments
    if len(argv) == 0:
        log("Not enough arguments", type="e")
        sys.exit(1)

    cmd = argv[0]
    log("Command: {}".format(cmd), type="d")
    data = load_config()
    if cmd == "ssh":
        sys.exit(cmd_ssh(argv[1:], data))
    elif cmd == "scp":
        sys.exit(cmd_scp(argv[1:], data))
    elif cmd == "list":
        cmd_list(argv[1:], data)
        sys.exit(0)
    elif cmd == "bind":
        cmd_bind(argv[1:], data)
    elif cmd == "view":
        sys.exit(cmd_view(argv[1:], data))
    elif cmd == "help":
        cmd_help()

if __name__ == "__main__":
    main()
